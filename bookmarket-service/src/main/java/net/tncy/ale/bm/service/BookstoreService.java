package net.tncy.ale.bm.service;

import net.tncy.ale.bm.data.Bookstore;
import net.tncy.ale.bm.data.InventoryEntry;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class BookstoreService {

    private final BookService bookService;

    public BookstoreService(BookService bookService) {
        this.bookService = bookService;
    }

    public int createBookstore(String name) {
        return -1;
    }

    public void deleteBookstore(int bookstoreId) {
    }

    public Bookstore getBookstoreById(int bookstoreId) {
        return null;
    }

    public List<Bookstore> findBookByName(String name) {
        return null;
    }
    
    public void renameBookstore(int bookstoreId, String newName) {
    }

    public void addBookToBookstore(int bookstoreId, int bookId, int quantity, float price) {
    }

    public void removeBookFromBookstore(int bookstoreId, int bookId, int quantity) {
    }

    public List<InventoryEntry> getBookstoreInventory(int bookstoreId) {
        return null;
    }

}
