package net.tncy.ale.bm.service;

import net.tncy.ale.bm.data.Book;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class BookService {

    public int createBook(String title, String publisher, String isbn) {
        return 0;
    }

    public void deleteBook(int bookId) {
    }

    public Book getBookById(int bookId) {
        return null;
    }

    public List<Book> findBookByTitle(String title) {
        return null;
    }

    public List<Book> findBookByAuthor(String author) {
        return null;
    }

    public Book getBookByIsbn(String isbn) {
        return null;
    }
}
