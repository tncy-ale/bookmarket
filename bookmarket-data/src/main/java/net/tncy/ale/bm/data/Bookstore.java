package net.tncy.ale.bm.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Bookstore {

    private int id;
    private String name;
    private List<InventoryEntry> inventoryEntries = new ArrayList<>();

    public Bookstore(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<InventoryEntry> getInventoryEntries() {
        return inventoryEntries;
    }

    public void addInventoryEntry(InventoryEntry inventoryEntry) {
        this.inventoryEntries.add(inventoryEntry);
    }

    @Override
    public String toString() {
        return "Bookstore{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", inventoryEntries=" + inventoryEntries +
                '}';
    }
}
